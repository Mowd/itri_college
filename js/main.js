function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
}

IC = {
    SwapImage: function() {
        $("img[swap]").on("mouseenter", function() {
            $(this).attr("orig", $(this).attr("src"));
            $(this).attr("src", $(this).attr("swap"));
        }).on("mouseleave", function() {
            $(this).attr("src", $(this).attr("orig"));
        });
    },
    GA: {
        setCallback: function() {
            IC.GA.menu();
            IC.GA.recordButton();
        },
        menu: function() {
            $(".nav-link").on("click", function() {
                console.log($(this).text());
                ga('send','event','menu',"click",$(this).text());
            });
        },
        recordButton: function() {
            $(".purple-small-btn, .purple-btn, .red-btn").on("click", function() {
                console.log($(this).parent().attr("href"));
                ga('send','event','button',"click",$(this).parent().attr("href"));
            });
            $(".purple-border-btn").on("click", function() {
                console.log($(this).attr("data-target"));
                ga('send','event','course',"click",$(this).attr("data-target"));
            });
        }
    }
};