function getRandomIntInclusive(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min); //The maximum is inclusive and the minimum is inclusive
}

IC = {
    SwapImage: function() {
        $("img[swap]").on("mouseenter", function() {
            $(this).attr("orig", $(this).attr("src"));
            $(this).attr("src", $(this).attr("swap"));
        }).on("mouseleave", function() {
            $(this).attr("src", $(this).attr("orig"));
        });
    },
    GA: {
        setCallback: function() {
            IC.GA.menu();
            IC.GA.redButton();
        },
        menu: function() {
            $(".nav-link").on("click", function() {
                console.log($(this).text());
                ga('send','event','menu',"click",$(this).text());
            });
        },
        redButton: function() {
            $(".red-small-btn").on("click", function() {
                console.log($(this).parent().attr("href"));
                ga('send','event','red_button',"click",$(this).parent().attr("href"));
            });
        }
    },
    Game: {
        setCallback: function() {
            IC.Game.chooseCard();
            IC.Game.confirmCard();
            IC.Game.toss();
        },
        chooseCard: function() {
            $(".choose-card-btn").on("click", function() {
                ga('send','event','game','start');
                $(".burner-div, .choose-card-btn").fadeOut(function() {
                    $(".tip-text1").fadeIn();
                    $(".cow-div").fadeIn();
                    $(".confirm-card-div").fadeIn();
                });
            });
        },
        confirmCard: function() {
            $(".cow-card").on("click", function() {
                ga('send','event','game','pick_card');
                $(".cow-div").fadeOut(function() {
                    $(".tip-text1").fadeOut(function() {
                        $(".tip-text2").fadeIn();
                    });
                    $(".toss-div").fadeIn();
                });
            });
        },
        toss: function() {
            var res_num = getRandomIntInclusive(1, 20);
            $(".toss-btn").on("click", function() {
                ga('send','event','game','end', res_num);
                $(".res-sign").css("background-image", "url(../images/exam/sign" + res_num.toString() + "_@2x.png)");
                $(".res-sign-paper").attr("src", "../images/exam/SignPaper_" + res_num.toString() + "_@2x.png");
                $(".toss-div").fadeOut(function() {
                    $(".tip-text2").fadeOut(function() {
                        $(".tip-text3").fadeIn();
                    });
                    $(".res-div").fadeIn();
                });
            })
        }
    }
};